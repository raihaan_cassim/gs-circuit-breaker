package reading;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Class that reads. It's a good student.
 * 
 * @author raihaan
 *
 */
@EnableCircuitBreaker
@RestController
@SpringBootApplication
public class ReadingApplication {

	@Autowired
	private BookService bookService;

	@RequestMapping(value = "/to-read")
	public String toRead() {
		return bookService.readingList();
	}

	/*@RequestMapping(value = "/to-read")
	public String readingList() {
		RestTemplate restTemplate = new RestTemplate();
		URI uri = URI.create("http://localhost:8090/recommended");
		return restTemplate.getForObject(uri, String.class);
	}*/

	public static void main(String[] args) {
		SpringApplication.run(ReadingApplication.class, args);
	}

}
