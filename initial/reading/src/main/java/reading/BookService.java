package reading;

import java.net.URI;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

/**
 * Implement the circuit breaker!
 * 
 * @author raihaan
 *
 */
@Service
public class BookService {

	@HystrixCommand(fallbackMethod="reliable")
	public String readingList() {
		RestTemplate restTemplate = new RestTemplate();
		URI uri = URI.create("http://localhost:8090/recommended");
		return restTemplate.getForObject(uri, String.class);
	}

	public String reliable() {
		return "Cloud Native Java";
	}
	
}
